import click
import csv
import os

import util
import util.api_client as api_client

ALLOW_DUPLICATES = True


def get_all_related_items(path):
    full_path = os.path.expanduser(path)
    full_path = os.path.expandvars(full_path)
    result = []
    with open(full_path) as csvfile:
        csvReader = csv.DictReader(csvfile)
        for row in csvReader:
            result.append(row)
    return result


@click.command()
@click.option('--path', help='Path to json file for member questions')
@click.option('--base-url', help='For example https://demo-data.humdata.org')
@click.option('--api-key', help='CKAN API KEY')
def main(path, base_url, api_key):
    '''

    '''
    # determine whether migration should allow duplicates
    allow_duplicates = ALLOW_DUPLICATES

    related_items = get_all_related_items(path)

    # related items must have unique titles before migration
    duplicate_titles = util.manage_duplicate_titles(related_items)
    if duplicate_titles and allow_duplicates == False:
        print(
            """All Related Items must have unique titles before migration. The following
Related Item titles are used more than once and need to be corrected before
migration can continue. Please correct and try again:"""
        )
        for i in duplicate_titles:
            print(i)
        return


    related_items_num = len(related_items)
    for i, related in enumerate(related_items):

        showcase_title = util.gen_new_title(related.get('title'))
        title_for_name = util.gen_new_title(related.get('modified_title')) if related.get(
            'modified_title') else showcase_title
        data_dict = {
            'original_related_item_id': related.get('id'),
            'title': related['title'],
            'name': util.munge_title_to_name(title_for_name),
            'notes': related.get('description'),
            'image_url': related.get('image_url'),
            'url': related.get('url'),
            'tags': [{"name": related.get('type').lower()}]
        }
        # make the showcase
        push_to_ckan(api_key, base_url, data_dict, i, related.get('dataset_id'), related_items_num, True)


def push_to_ckan(api_key, base_url, data_dict, index, dataset_id, related_items_num, try_again):

    normalized_title = util.substitute_ascii_equivalents(data_dict['title'])

    try:
        new_showcase = api_client.create_showcase(data_dict, base_url, api_key)
    except Exception as e:
        print('There was a problem migrating "{}" - {}: {}. Try again ? {}'.format(
            normalized_title, data_dict['name'], e, try_again))
        if try_again:
            # There is already a dataset with the same name
            data_dict['name'] += '-showcase'
            push_to_ckan(api_key, base_url, data_dict, index, dataset_id, related_items_num, False)
    else:
        print('Created Showcase {}/{} from the Related Item "{}" - {}'.format(
            index + 1, related_items_num, normalized_title, data_dict['name']))

        # make the showcase_package_association, if needed
        try:
            related_pkg_id = dataset_id
            if related_pkg_id:
                rel_data_dict = {
                    'showcase_id': new_showcase['id'],
                    'package_id': related_pkg_id}
                print ('Adding dataset {} to showcase {}'.format(related_pkg_id, data_dict['name']))
                api_client.add_dataset_to_showcase(rel_data_dict, base_url, api_key)

        except Exception as e:
            print('There was a problem creating the showcase_package_association for "{0}": {1}'.format(
                normalized_title, e))


if __name__ == '__main__':
    main()
