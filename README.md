SCRIPT FOR IMPORTING CKAN RELATED ITEMS TO SHOWCASES
====================================================

Steps:

 1. Export related item information to CSV: `\copy (SELECT r.*, rd.dataset_id FROM related r, related_dataset rd WHERE r.id = rd.related_id and rd.status = 'active') TO '/tmp/export.csv' CSV HEADER`
 2. Install python requirements from *requirements.txt*
 3. Run the script to import the file `--path /tmp/export.csv --api-key API_KEY --base-url http://data.humdata.local`
 
 
# Notes 

It's based on the showcase migrate script from [here](https://github.com/ckan/ckanext-showcase/blob/master/ckanext/showcase/commands/migrate.py)