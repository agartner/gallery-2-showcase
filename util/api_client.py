import requests
import json

def create_showcase(data_dict, base_url, api_key):
    r = requests.post(base_url + '/api/action/ckanext_showcase_create',
                      data=json.dumps(data_dict),
                      headers={'Authorization': api_key, 'content-type': 'application/json'},
                      verify=False)

    r.raise_for_status()
    return r.json().get('result', {})

def add_dataset_to_showcase(data_dict, base_url, api_key):
    r = requests.post(base_url + '/api/action/ckanext_showcase_package_association_create',
                      data=json.dumps(data_dict),
                      headers={'Authorization': api_key, 'content-type': 'application/json'},
                      verify=False)
    r.raise_for_status()

    return r.json().get('result', {})